# Specs

What the project is, what it aims to achieve and how

# What

In the document Danewb will be abbreviated as DNB.

Danewb is a **web app** browser project.


# Motivation

Most web "pages" you browse nowadays are Single Page Applications (SPA).
They provide some of the functionalities of a normal app more slowly, more power-intensively, 
 limited to a few programming languages, and in convoluted ways.

## Browsers are web operating systems

Whatever version of the web we are at now, web browsers now look so much like operating systems (OS),
 that there have been attempts at building operating systems with/around them.
Websites are becoming applications, but with severely limited access to resources and methods to draw their UI components, restricted communication methods, and development languages.
In fact, they are quite comparable to applications running in a container that sandboxes them from the host OS.

## Power imbalance

At the time of this writing, the browser landscape is heavily lopsided towards one big player with few competitors.
Many supposed competitors are using the engine powering the big player (written of course by the big player).
This only furthers the power imbalance and helps limit web browsers + makes them dependent on one entity.

# Principles

## The OS is the only OS

Instead of running web apps in a quasi-OS, running into all the problems the OS runs into and reinventing the wheel,
 DNB will run apps on the host OS in containers provided by the OS.
This will reduce DNB to a framework to download applications, put them into their specified container,
 secure the container, provide a drawing space for the UI, and start the container.


## Real Web Applications

Without trying to invoke the "Real Scottsman Fallacy", DNB applications are to become runnable applications.
Leeway will be provided to the application developer as to which container they want to execute in, thus
 delegating the decision on bundling dependencies or declaring them for the framework to take care of.


# How

Now comes the hard part; and this is WIP.
What follows is a general overview of how things might work.
More in-depth analysis will follow in subdocuments.

## General flow

 - User tries to open a DNB URI e.g `dnb://somehost.test`
 - Desktop opens program in charge of DNB URIs: `dnb`
 - By default `dnb` will translate these to HTTP URIs
 - `dnb` now:
   * Retrieves the app descriptor `http://somehost.test/dnb-dep.json` (`dnb` can be extended to support other formats)
     + Deps can be required access to specific system resources, which container image to use, the command to execute in the container, etc.
     + User might be asked permissions access to certain resources
     + Bail if mandatory permissions cannot be acquired
   * Configures the container to be run
     + Restricts access to resources
     + Provide a graphical surface to draw the app on (connect container to window manager or compositor)
   * Run app in container

The existing OS components to take care of window management, resource restrictions, and other aspects of the application's existence.

## Components

Following are the major parts of DNB and the OS components that will be communicated with

 - [BrApp]: Browser app that is actuall run
 - [BrAppTor]: BrApp dependencies
 - [canvas]: Where the BrApp is drawn on the screen (can be in another application)
 - [container]: Where the BrApp is run in
 - [DNB URI]s: Where path to BrApp is retrieved from
 - [`dnb`][dnb app]: THE app to orchestrate all of this
 

**Possible components**

These are components that might extend the scope of the project and make it more complicated; probably for later versions.

 - [Browser]: A traditional-style browser that calls `dnb`, allows navigating to URIs, provides canvases to draw BrApps in

[BrApp]: components/BrApp.md
[BrAppTor]: components/BrAppTor.md
[canvas]: components/canvas.md
[container]: components/container.md
[DNB URI]: components/URI.md
[dnb app]: components/app.md
