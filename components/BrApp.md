
# BrApp

A **Br**owser **App** is (unsurprisingly) and app that can be run by DNB.
In order to be run by DNB, it requires a sort of rubric (in other parts also called a manifest)
 which describes what the brapp is, does and requires to be run.
We call it a [BrAppTor].

# Running a BrApp

BrApps are run within containers - one app per container.
Depending on what's in the BrAppTor, a BrApp may be executed itself making it an executable,
 or be executed/interpreted by another program within the container.
It will get the arguments defined in the BrAppTor.

Whichever of the two, they are put into a container in a read-only manner and executed there.

**Main executable**

Examples of these are binaries.
The output of a program compiled from C/C++, C#, Brainfuck, D, Rust, etc. can be an example.

The first program ran in the container will be the main executable.

Comparable to an [`ENTRYPOINT`][ENTRYPOINT] in a `Dockerfile`.

**Passed as arg**

Scripts or even markup like HTML, and XML fit this pattern.
They are interpreted by a program in the container the brapp is run in.

Think of a python script that needs a python interpreter, a bash script that needs bash, 
 a java `.jar` that requires a java runtime, and so forth.

Comparable to a [`CMD`][CMD] in a `Dockerfile`.


[BrAppTor]: BrAppTor.md
[CMD]: https://docs.docker.com/engine/reference/builder/#cmd
[ENTRYPOINT]: https://docs.docker.com/engine/reference/builder/#entrypoint
