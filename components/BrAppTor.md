# BrAppTor

A **Br**owser **App** Descrip**tor** contains all the meta information about a [BrApp].
It's comparable to a [`Cargo.toml`][Cargo.toml], `manifest.json`, [`package.json`][package.json], and others. 

The descriptor is made up of sections that themselves have either subsections or fields.

# Sections

## Basic

 - Name
 - Authors (at least one)
 - Description (optional)
 - License

## Containers

A list of containers the BrApp can run in with their different configs.
This allows DNB to choose one depending on what the user prefers.

### Type

See the [container] page for the possible ways an app can be contained.


### Config

Depending on the type of container selected, there will be different fields here.

## Permissions

A list of permissions can be requested by the BrApp e.g access to microphone, camera, harddrdive, network, etc.
They themselves have these fields:

 - Mandatory: `true` by default.  
   If a mandatory permissions is rejected, the user should be averted that the app might not work well.

`TODO: Add list of possible permissions`


[Cargo.toml]: https://doc.rust-lang.org/cargo/reference/manifest.html
[container]: container.md
[package.json]: https://docs.npmjs.com/cli/v7/configuring-npm/package-json
