# DNB URI

The Danube Uniform Resource Identifier ([URI]) provides an entry point to a domain.
It's used in order to be able to trigger the opening of the [`dnb`][dnb] application.

Operating systems can register different applications to different protocols, which is the targeted mechanism.

Behind the scenes `dnb` will use another protocol to actually retrieve resources.
By default, that will be HTTP.

## Format

`dnb[+<other scheme>]://[<host>]<path>`

If supported by [`dnb`][dnb], `other scheme` will serve as the protocol used to access the resources on given host.
Examples are `http`, `file`, `ftp`, etc.

If a library is used (e.g `libcurl`) it might be possible to support a wide range from the getgo.


[dnb]: app.md
[URI]: https://en.wikipedia.org/wiki/Uniform_Resource_Identifier
