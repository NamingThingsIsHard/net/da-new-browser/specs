# The `dnb` app

This is the core component of Danube and does the heavy lifting.
In short, it:

 - retrieves a [BrApp] + [BrApp descriptor][BrAppTor]
 - Prepares environment and container for executing BrApp
 - Executes BrApp

## BrApp and descriptor retrieval

Given a DNB URI e.g `dnb://somehost.test/at/some/path`, `dnb` first has to retrieve the BrAppTor.
Since nobody actually speaks `dnb://`, another protocol has to be used to access resources locally or remotely.
By default, that protocol will be `http`.

The following URIs will be constructed to retrieve the resources:

| resource | URI |
| -------- | --- |
| BrAppTor | http://somehost.test/at/some/path |
| BrApp    | http://somehost.test/at/some/path/brapp |


BrAppTor will be assumed to be `JSON`, but different protocols will be able to pass headers specifying the type.

## Environment preparation

TODO:

 - providing access to a canvas (see [x11docker])

## Container preparation

TODO

 - resource constriction (memory, CPU, storage)
 - anti-tracking: CPU version, available host storage, GPU, etc.
   * can the CPU be spoofed to be generic?
 - what kind of containers exist outside linux and which features do they have in common?

[BrApp]: BrApp.md
[BrAppTor]: BrAppTor.md
[x11docker]: https://github.com/mviereck/x11docker
