# The canvas

This is where [BrApps][BrApp] will draw their contents.

It will be up to the user or app to decide what kind of canvas a BrApp should get.

# Possible canvases

Canvases can exist anywhere - on a real or virtual screen.
Differences arise in the contexts those canvases were created and how big they are.

The following contexts can exist (ordered by least restrictive)

## 1. Full host display access

With access at this level, BrApps have the most permissions and it would nearly be the equivalent of given it full access; a BrApp sandbox might as well not exist in this mode,
 since breaking out would be much easier.

An example of this would be passing the main X11 socket with read-write access.
BrApps would be able to tamper with their environment at will.


# 2. A virtual display

Virtual displays may be offscreen or in windows.
A canvas therein would have full access to the virtual display, but not the host display containing the former.


## 3. Full window access

Canvases in this form span the entire window.
BrApps should be able to define the look of the window by sending hints to the [compositor].

## 4. App controlled canvas

This is like #3 with the difference being that the canvas is restricted to what the host app wants it to. It would be the similar to a page in a browser. The browser controls the tabs, status bar, yadayada, but cedes limited control to the webpage. In Danube, the BrApp could have full control of the canvas.


[BrApp]: BrApp.md
[compositor]: https://en.wikipedia.org/wiki/Compositing_window_manager
