# Container

Containers allow launching applications in a controlled environment with bundled dependencies.
In the context of DNB, no distinction is made between linux containers and virtual machines.
DNB clients are free to implement their own method of sandboxing or containing a BrApp.

A few candidates are:

 - Docker
 - Flatpak
 - Snap

Even virtual machines or Qemu can be used if DNB clients so wish.
